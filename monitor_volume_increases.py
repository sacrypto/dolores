#!/usr/bin/env python

import re
import time
import json
import urllib2

from BeautifulSoup import BeautifulSoup

from slackbot import SlackComms


def main():
    # Amount of days we are comparing.
    days = 7

    # Pull all currincies data from coinmarketcap
    currency_data =  get_marketcap_data()
    slack_client = SlackComms()

    # Not so good implementation of logging for now...
    start_message = "Started monitoring: {}".format(time.strftime("%Y-%m-%d %H:%M:%S"))
    slack_client.post_message("##### " + start_message + " #####")
    print start_message

    # Temporary suppression list of currencies we do not want to alarm on.
    muted_coins = ["steem", "iota", "bytom", "dragonchain", "nav-coin", "groestlcoin", "ubiq", "achain", "melon", "viacoin", "whitecoin", "bodhi", "steem-dollars", "blockv", "hempcoin", "triggers", "omni", "radium", "suncontract", "vcash", "ark", "verge", "centra", "wagerr", "unikoin-gold", "storj", "vechain", "walton", "monero", "decision-token", "goldcoin", "tether", "eos", "dash"]

    for coin in currency_data:
        # This is in a try/except because some of the data we pull is null and
        # we'll just skip if there's any issues. Most issues are with new
        # currencies that don't have much backing at all..
        try:
            # Skip currencies we do not want to check for whatever reason,
            # skip currencies with low 24 trade volume,
            # and skip currencies that have a low market cap
            if [x for x in muted_coins if x in coin["id"]]:
                continue
            if float(coin["24h_volume_usd"]) < 500000:
                continue
            if float(coin["market_cap_usd"]) < 1000000:
                continue

            # Use the volume to find the average of the last N days
            curr_vol = float(coin["24h_volume_usd"])
            hist_vol_avg = find_avg(collect_hist_data(days, coin["id"]))
            vol_increase_percent = find_current_vol_increase(curr_vol,
                                                             hist_vol_avg)

            # Restrict the amount of increase we want to report on
            if vol_increase_percent >= 70 and vol_increase_percent <= 200:
                output_coin_data(coin, vol_increase_percent, slack_client)
        except Exception as e:
            continue

    exit_message =  "Execution time: {} seconds. {}".format(time.time()-start, time.strftime("%Y-%m-%d %H:%M:%S"))
    slack_client.post_message("##### " + exit_message + " #####" + "\n")
    print exit_message


def output_coin_data(data, increase, client):
    """Prints coin data to STDOUT."""
    coin_url = "https://coinmarketcap.com/currencies/{}/".format(data["id"])
    message =  "{}({}) volume at {}%\n\t{}\n".format(data["id"],
                                                     data["symbol"],
                                                     str(increase).split('.')[0],
                                                     coin_url)
    client.post_message(message)


def get_marketcap_data():
    """Gets all currency data from coinmarketcap.com and returns a dict."""
    url = "https://api.coinmarketcap.com/v1/ticker?start=0&limit=0"
    r = urllib2.urlopen(url)
    return json.loads(r.read())


def find_avg(hist_data):
    """Calculates the average of a list."""
    sum = 0
    for day in hist_data:
        sum += day
    return sum / len(hist_data)


def find_current_vol_increase(current_volume, hist_avg_vol):
    """Calculates the percentage of increase."""
    return (current_volume - hist_avg_vol) / hist_avg_vol * 100


def collect_hist_data(days, coin_name):
    """Parses historical data from HTML and returns a list of floats."""
    hist_url = "https://coinmarketcap.com/currencies/{}/historical-data/".format(coin_name)
    page = urllib2.urlopen(hist_url)
    soup = BeautifulSoup(page)

    row = soup.body.find('table', attrs={'class': 'table'}).findAll('tr', attrs={'class': 'text-right'})

    week_vol = []
    for td in row[0:days + 1]:
        html_text = td.findAll('td')[5].text
        week_vol.append(float(re.sub('<[^<]+?>', '', html_text).replace(',', '')))

    return week_vol


if __name__ == "__main__":
    start = time.time()
    main()
