#!/usr/bin/env python

import json

from slackclient import SlackClient

class SlackComms(object):
    def __init__(self):
        with open('/home/nom/py/slack.json') as f:
            slack_creds = json.load(f)
        self.oauth_token = slack_creds["oauth_token"]
        self.bot_token = slack_creds["bot_token"]
        self.sc = SlackClient(self.oauth_token)

    def post_message(self, message):
        channel_id = "C89J27JHL"
        method = "chat.postMessage"
        response = self.sc.api_call(method, channel=channel_id, token=self.bot_token, text=message)
        return response
